import { Component } from '@angular/core';
import {AuthService} from "./auth/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'canalApp';
  isAuth = false;
  userName: string = '';
  sortBy: string = 'Title';
  showFilter = false;
  constructor(private authService: AuthService) {
    this.authService.isAuth$.subscribe( isAuth => {
      if (isAuth) {
        this.isAuth = true;
        this.userName = this.authService.userName;
      } else {
        this.isAuth = false;
        this.userName = '';
      }
    })
  }

  selectItem(value: string) {
    this.sortBy = value;
    this.toggle();
  }
  toggle() {
    this.showFilter = !this.showFilter;
  }
  deconnexion() {
    this.authService.deconnexion();
  }
}
