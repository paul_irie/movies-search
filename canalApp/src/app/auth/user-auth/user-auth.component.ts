import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth.service";

@Component({
  selector: 'canal-user-auth',
  templateUrl: './user-auth.component.html',
  styleUrls: ['./user-auth.component.css']
})
export class UserAuthComponent implements OnInit {
  name: string = '';
  password: string = ''
  onerror = false;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  connexion() {
    if (this.name && this.password) {
      this.authService.connexion({username: this.name, password: this.password}).subscribe(value => {
        if (value && value.token) {
          this.authService.storeUser(value.token, this.name)
        }
      }, () => {
        this.onerror = true;
      });
    }

  }
}
