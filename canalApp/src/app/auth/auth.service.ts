import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {LoginBody} from "./user.model";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  AUTH_API_URL = 'http://localhost:3000/auth/login';
  token: string = '';
  userName: string = '';
  isAuth$ = new BehaviorSubject<boolean>(false);
  constructor(
    private httpService: HttpClient
  ) {
    const token = window.sessionStorage.getItem('token');
    const name = window.sessionStorage.getItem('username');
    if (token && name) {
      this.userName = name;
      this.token = token;
      this.isAuth$.next(true);
    }
  }

  connexion(credential: LoginBody) {
    /*let headers = new HttpHeaders();
    headers.set('content-type', 'application/json')
    headers.set('Access-Control-Allow-Origin', '*');*/
    /*headers.append('Authorization', 'Basic ' +
      btoa('username:password'));*/
    return this.httpService.post<{ token: string } >(this.AUTH_API_URL, credential);
  }

  deconnexion() {
    this.isAuth$.next(false);
    window.sessionStorage.removeItem('token');
    window.sessionStorage.removeItem('username');
    this.token = '';
    this.userName = '';
  }

  storeUser(token: string, name: string) {
    if (token) {
      this.userName = name;
      this.token = token;
      this.isAuth$.next(true);
      window.sessionStorage.setItem('token', token);
      window.sessionStorage.setItem('username', name);
    }
  }
}
