import { Injectable } from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {catchError, throwError} from "rxjs";

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const accessToken = this.authService.token;

    if (accessToken) {
      const tokenizedReq = req.clone({
        setHeaders: {
          Authorization: `Bearer ${accessToken}`
        }
      });
      return next.handle(tokenizedReq).pipe(
        catchError(err => {
          if (err.status === 401) {
            this.authService.deconnexion();
          }
          return throwError(err);
        })
      );
    }
    return next.handle(req);
  }
}
