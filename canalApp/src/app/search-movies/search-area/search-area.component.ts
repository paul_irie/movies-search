import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {SearchService} from "../search.service";
import {FormControl} from "@angular/forms";
import {debounce, debounceTime, delay, map, Subscription, timeout} from "rxjs";
import { Movie } from '../movie.model';

@Component({
  selector: 'canal-search-area',
  templateUrl: './search-area.component.html',
  styleUrls: ['./search-area.component.css']
})
export class SearchAreaComponent implements OnInit, OnDestroy {
  @Input('sortBy') set fnsortBy (sortBy: string) {
    this.sortBy = sortBy;
    this.getMovies(this.input.value);
  }
  input: FormControl = new FormControl('');
  movies: Movie[] = [];
  selectedIndex: number = -1;
  sortBy: string = 'Title';
  subs: Subscription[] = [];

  constructor(private searchService: SearchService) { }

  ngOnInit(): void {
    this.subs.push(
      this.input.valueChanges.pipe(debounceTime(1200)).subscribe(value => {
        this.getMovies(value);
      })
    );
  }

  getMovies (value: string) {
    if (value?.length >= 3) {
      this.searchService.getMovies$(value, this.sortBy).subscribe((values: Movie[]) => {
        this.movies = values;
      })
    } else {
      this.movies = [];
    }
  }

  inc (i: number) {
    return i = i + 1;
  }

  selectItem(i: number) {
    if (i !== this.selectedIndex) {
      this.selectedIndex = i;
    } else {
      this.selectedIndex = -1;
    }

  }

  isSelectedIndex(i: number) {
    return this.selectedIndex === i;
  }
  ngOnDestroy() {
    this.subs.forEach(s => s.unsubscribe());
    this.subs.length = 0;
  }

}
