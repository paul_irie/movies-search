import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchMoviesRoutingModule } from './search-movies-routing.module';
import { SearchAreaComponent } from './search-area/search-area.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
    declarations: [
        SearchAreaComponent
    ],
    exports: [
        SearchAreaComponent
    ],
    imports: [
        CommonModule,
        SearchMoviesRoutingModule,
      ReactiveFormsModule
    ]
})
export class SearchMoviesModule { }
