import { Injectable } from '@angular/core';
import { Observable, of} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../auth/auth.service";
import {Movie} from "../search-movies/movie.model"

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  SEARCH_API_URL = 'http://localhost:3000/movies/';
  constructor(
    private httpService: HttpClient,
    private authService: AuthService
  ) { }

  getMovies$(query: string, sortBy: string): Observable<Movie[]> {
    if (this.authService.token) {
      return this.httpService.get<Movie[]>(this.SEARCH_API_URL + `?query=${query}&sortBy=${sortBy}`);
    }
    return of([] as Movie[]);
  }
}
